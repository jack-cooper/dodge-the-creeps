# Dodge the Creeps

The result of following Godot's [Dodge the Creeps](https://docs.godotengine.org/en/stable/getting_started/step_by_step/your_first_game.html) tutorial and exporting for web.

Play the game [here](https://jack-cooper.gitlab.io/dodge-the-creeps/)!
